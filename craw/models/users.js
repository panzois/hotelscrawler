const Model = require('./model');

// ======================================================================================================
const modelName = 'Tripadvisor_hotels';

module.exports = new Model({
    name: modelName,
    fields: {
      schema: {
        name: {
          type: String,
        },
        reviews: {
          type: Array,
        },
        evaluations: {
            type: Array,
        },
      },
    },
});