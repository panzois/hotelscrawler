var request = require('request');
var cheerio = require('cheerio');
const keyArray= ["διανυκτερεύσεις"];
const stars = 4;
// =======================================================================================================
function doRequest(url) {
    return new Promise(function (resolve, reject) {
      request(url, function (error, res, body) {
        if (!error && res.statusCode == 200) {  
            resolve(body);
        } else {
            reject(error);
        }
      });
    });
  }
//========================================================================================================

// =================================================================
async function findReview(manyReviewUrls){
        var evaluations = [];
        var reviews = [];
        var reviewPromises = [];
        reviewPromises.length = manyReviewUrls.length;
        reviewPromises = manyReviewUrls.map( (url)=>( doRequest(url)));
        const reviewBodies = await Promise.all(reviewPromises);
        reviewBodies.forEach(function(body,index){
            var $ = cheerio.load(body);
            // for stars of review
            var stars=$(' .nf9vGX55   ');
            stars.each(function(){
                // text of review
                var starNum;
                if($(this).find(' .bubble_50 ').length!==0){
                    starNum=5;
                }else if($(this).find(' .bubble_40 ').length!==0){
                    starNum=4;
                }else if($(this).find(' .bubble_30 ').length!==0){
                    starNum=3;
                }else if($(this).find(' .bubble_20 ').length!==0){
                    starNum=2;
                }else if($(this).find(' .bubble_10 ')!==0){
                    starNum=1;
                }
                evaluations.push(starNum);
            });
            // for boxes of review
            var review=$(' ._2wrUUKlw  ');
            review.each(function(){
                // text of review
                review=$(this).find(' .IRsGHoPm ').text();
                reviews.push(review);
            });
        });
        // console.log(evaluations);
        // console.log(reviews);
        filter(reviews,evaluations,keyArray, stars);
    }
    // =================================================================
    
    function filter(reviews,evaluations,wordArray, stars){
        var wordfiltered = [];
        var starfiltered = [];
        if(stars!==undefined){
            evaluations.forEach(function(evaluation,index){
                if(evaluation===stars){
                    starfiltered.push(reviews[index]);
                }
            });
        }
        if( starfiltered.length===0){
            starfiltered = reviews;
        }
        if(wordArray.length!==0){
            starfiltered.forEach(function(review){
                for(var i=0; i<wordArray.length; i++){
                    if(review.search(wordArray[i])!==-1){
                        wordfiltered.push(review);
                    }
                }
            });
        }
        if(wordfiltered.length===0){
            wordfiltered=starfiltered;
        }
        
        console.log(wordfiltered);
        console.log(wordfiltered.length);
    }
    // =================================================================
    var words=[];
    findReview([
        'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-Castello_City_Hotel-Heraklion_Crete.html#REVIEWS',
        'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or5-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
        'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or10-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
        'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or15-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
        'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or20-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    ]);

    
    
    
    
    // [
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-Castello_City_Hotel-Heraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or5-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or10-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or15-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or20-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or25-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or30-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or35-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or40-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or45-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or50-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or55-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or60-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or65-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or70-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or75-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or80-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or85-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or90-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or95-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or100-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or105-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or110-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or115-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or120-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or125-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or130-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or135-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or140-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or145-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or150-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or155-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or160-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or165-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or170-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or175-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or180-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or185-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or190-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or195-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or200-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or205-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or210-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or215-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or220-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or225-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or230-Castello_City_HotelHeraklion_Crete.html#REVIEWS',
    //     'http://www.tripadvisor.com.gr/Hotel_Review-g189417-d491536-Reviews-or235-Castello_City_HotelHeraklion_Crete.html#REVIEWS'
    // ]